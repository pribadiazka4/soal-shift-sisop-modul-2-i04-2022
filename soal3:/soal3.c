#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <sys/types.h>


int main() {
    pid_t childID;
    int stats;
    childID = fork();

    if (childID < 0) {
        exit(EXIT_FAILURE);
    }

    if (childID == 0) {
    char *argv[] = {"mkdir", "-p", "/home/bamz/modul2/darat", NULL };
    execv("/bin/mkdir", argv);
    } else {
    while ((wait(&stats)) > 0);
    char *argv[] = {"mkdir", "-p", "/home/bamz/modul2/air", NULL};
    execv("/bin/mkdir", argv);
    }

}

#include <unistd.h>
#include <wait.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

 
int main()
{
	pid_t childID_1 = fork();
 	if(childID_1 == 0)
	{
    	char *mkdir[] = {"mkdir", "-p", "gacha_gacha", NULL};
    	execv("/bin/mkdir", mkdir);
	}
 
	pid_t childID_2 = fork();
 	if(childID_2 == 0 )
	{
    	char *args[]={"wget", "-q", "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "genshin_weapon.zip", NULL};
    	execv("/bin/wget", args);
	}

	pid_t childID_3 = fork();
 	if(childID_3 == 0 )
	{
    	char *args[]={"wget", "-q", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "genshin_characters.zip", NULL};
    	execv("/bin/wget", args);
	}
 
	sleep(10);
 
	pid_t childID_4 = fork();
 	if(childID_4 == 0 )
	{
      execlp("unzip","unzip","-q","/home/bamz/genshin_weapon.zip",NULL);
	}

	pid_t childID_5 = fork();
 	if(childID_5 == 0 )
	{
      execlp("unzip","unzip","-q","/home/bamz/genshin_characters.zip",NULL);
	}

}
